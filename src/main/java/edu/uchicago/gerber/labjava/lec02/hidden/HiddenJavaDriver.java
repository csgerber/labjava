package edu.uchicago.gerber.labjava.lec02.hidden;

public class HiddenJavaDriver {

    /*
    Hidden Java:
import java.lang.*;
extends Object / Other
no-arg constructor

     */

    public static void main(String[] args) {
        HiddenJavaDriver hiddenJava = new HiddenJavaDriver();
    }

}
