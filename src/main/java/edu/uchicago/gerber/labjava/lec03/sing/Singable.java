package edu.uchicago.gerber.labjava.lec03.sing;

public interface Singable {
   public abstract String sing();
   String dance();
}
